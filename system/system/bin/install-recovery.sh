#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:52c8acd2a42bf7f5e17c1d3de74f7b1d91e2b5f4; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:bb596a6f5c565797bd5c9010bca91600fcea74aa \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:52c8acd2a42bf7f5e17c1d3de74f7b1d91e2b5f4 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
